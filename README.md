# Gestión de Socios de la Central de Vendedores de Amazon
El sistema deberá proporcionar información completa sobre los proveedores de la Central de Vendedores, mostrar los proveedores, poder guardarlos, modificarlos y borrarlos cuando sea necesario.

La base de datos para el sistema se encontrará en ¸`database/Sistema.sql`

Los datos de plataforma son:
+ PHP v7.3
+ Laravel 7.*
+ MariaDB 10.1.14

ToDo
+ [x] Montar la BD
+ [] Poblar la BD
+ [x] Dar permisos a colaboradores
+ [] Crear el primer CRUD (escoger)
+ [] Decidir si el sistema tendrá login
+ [] Arreglar los valores de .env para la BD

