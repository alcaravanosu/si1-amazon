<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// pruebas
Route::get('/poblar/auditoria', 'AuditoriaController@poblar_auditoria');
Route::get('/prueba', 'PruebaController@index');
//Route::get('/pruebita', 'PruebaController@mundo');
Route::get('/pruebita', function () {
    return view('welcome');
});

// Vista
Route::get('/usuarios', 'UsuarioController@index')->name('usuarios');


// Crear
Route::get('/usuarios/create', 'UsuarioController@create')->name('usuarios.create');
// Actualizar
// Borrar

// Socios
Route::resource('/socios', 'SocioController');
