@extends('layouts.app')
@section('content')

    <div class="row">

        <div class="col-lg-12 margin-tb">

            <div class="pull-left">

                <h2>Amazon - Usuarios</h2>

            </div>

            <div class="pull-right">

                <a class="btn btn-success" href="{{ route('usuarios') }}">Crear registro</a>

            </div>

        </div>

    </div>



    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif



    <table class="table table-bordered">

            <thead>
            <tr class="text-center">
               <th class="text-center">No</th>
               <th class="text-center">Name</th>
               <th class="text-center">Email</th>
               <th class="text-center">Movil</th>
               <th class="text-center">Patron</th>
               <th class="text-center">Bloqueo</th>
               <th class="text-center">Activo</th>
               <th class="text-center">Action</th>
            </tr>
            </thead>

        @forelse ($usuarios as $usuario)

        <tr>
                <td class="text-center">{{ $loop->index + 1 }}</td>
                <td class="text-center">{{ $usuario->Nb_Usuario }}</td>
                <td class="text-center">{{ $usuario->Tx_Email }}</td>
                <td class="text-center">{{ $usuario->Nu_Movil }}</td>
                <td class="text-center">{{ $usuario->Tx_Patron }}</td>
                <td class="text-center">{{ $usuario->St_Bloqueo }}</td>
                <td class="text-center">{{ $usuario->St_Activo }}</td>

            <td>

                <form action="{{ route('usuarios',$usuario->Co_Usuario) }}" method="POST">



                    <a class="btn btn-info" href="{{ route('usuarios',$usuario->Co_Usuario) }}">Ver</a>



                    <a class="btn btn-primary" href="{{ route('usuarios',$usuario->Co_Usuario) }}">Editar</a>



                    @csrf

                    @method('DELETE')



                    <button type="submit" class="btn btn-danger">Eliminar</button>

                </form>

            </td>

        </tr>

        @endforeach

    </table>






@endsection
<!-- ------------------------------------ -->
