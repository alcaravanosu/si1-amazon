@extends('layouts.app')
@section('content')

  <div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
          <h2>Editar Socio</h2>
        </div>
        <div class="pull-right">
          <a class="btn btn-primary" href="{{ route('socios.index') }}"> Regresar</a>
      </div>
    </div>
  </div>

  @if ($errors->any())
    <div class="alert alert-danger">
      <strong>Whoops!</strong> There were some problems with your input.<br><br>
      <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div>
  @endif

  <form action="{{ route('socios.update', $id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Name:</strong>
          <input type="text" name="Nb_Socio" class="form-control" value="{{ $socio->Nb_Socio }}">
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Identificación:</strong>
          <input type="text" name="Co_Identificacion" class="form-control" value="{{ $socio->Co_Identificacion }}">
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="form-group">
          <strong>Email:</strong>
          <input type="text" name="Tx_Email" class="form-control" value="{{ $socio->Tx_Email }}">
        </div>
      </div>

      <div class="col-xs-12 col-sm-12 col-md-12">
        <button type="submit" class="btn btn-success">Editar Socio</button>
      </div>
    </div>
  </form>
@endsection
