<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Socio</title>
</head>
<body>
  <h1>Socio</h1>
  <a href="{{ route('socios.index') }}">Regresar</a>

  <main>
    <p>
      <strong>ID: </strong>
      <span>{{ $socio->Co_Socio }}</span>
    </p>

    <p>
      <strong>Nombre: </strong>
      <span>{{ $socio->Nb_Socio }}</span>
    </p>

    <p>
      <strong>Identificación: </strong>
      <span>{{ $socio->Co_Identificacion }}</span>
    </p>

    <p>
      <strong>Email: </strong>
      <span>{{ $socio->Tx_Email }}</span>
    </p>

    <p>
      <strong>Movil: </strong>
      <span>{{ $socio->Nu_Movil }}</span>
    </p>

    <p>
      <strong>Calificación: </strong>
      <span>{{ $socio->Nu_Calificacion }}</span>
    </p>

    <p>
      <strong>Bloqueo: </strong>
      <span>{{ $socio->St_Bloqueo }}</span>
    </p>

    <p>
      <strong>Activo: </strong>
      <span>{{ $socio->St_Activo }}</span>
    </p>
  </main>
</body>
</html>
