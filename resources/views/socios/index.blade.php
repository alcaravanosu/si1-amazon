@extends('layouts.app')
@section('content')
    <div class="row">
      <div class="col-lg-12 margin-tb">
        <div class="pull-left">
          <h2>Amazon - Socios</h2>
        </div>
        <div class="pull-right">
          <a class="btn btn-success" href="{{ route('socios.create') }}">Crear Socio</a>
        </div>
      </div>
    </div>

    <table class="table table-bordered">
      <thead>
        <tr class="text-center">
          <th class="text-center">ID</th>
          <th class="text-center">Nombre</th>
          <th class="text-center">Email</th>
          <th class="text-center">Movil</th>
          <th class="text-center">Calificación</th>
          <th class="text-center">Bloqueo</th>
          <th class="text-center">Activo</th>
          <th class="text-center">Acción</th>
        </tr>
      </thead>

      <tbody>
        @foreach ($socios as $socio)
          <tr>
            <td class="text-center">{{ $loop->index + 1 }}</td>
            <td class="text-center">{{ $socio->Nb_Socio }}</td>
            <td class="text-center">{{ $socio->Tx_Email }}</td>
            <td class="text-center">{{ $socio->Nu_Movil }}</td>
            <td class="text-center">{{ $socio->Nu_Calificacion }}</td>
            <td class="text-center">{{ $socio->St_Bloqueo }}</td>
            <td class="text-center">{{ $socio->St_Activo }}</td>

            <td>
              <form action="{{ route('socios.destroy', $socio->Co_Socio) }}" method="POST">
                <a class="btn btn-info" href="{{ route('socios.show', $socio->Co_Socio) }}">Ver</a>
                <a class="btn btn-primary" href="{{ route('socios.edit', $socio->Co_Socio) }}">Editar</a>
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger">Eliminar</button>
              </form>
            </td>
          </tr>
        @endforeach
      </tbody>
    </table>
@endsection
