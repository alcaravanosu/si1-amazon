-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 172.18.0.3
-- Generation Time: Mar 19, 2021 at 06:43 PM
-- Server version: 10.1.14-MariaDB-1~jessie
-- PHP Version: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `amazon`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_03_04_181645_create_T00100_Usuario_table', 1),
(5, '2021_03_04_181645_create_T00200_Cargo_Organizacion_table', 1),
(6, '2021_03_04_181645_create_T00210_Socio_table', 1),
(7, '2021_03_04_181645_create_T00220_Socio_Cargo_Organizacion_table', 1),
(8, '2021_03_04_181645_create_T99999_Auditoria_table', 1),
(9, '2021_03_04_181645_create_T99999_Bitacora_table', 1),
(10, '2021_03_04_181646_add_foreign_keys_to_T00100_Usuario_table', 1),
(11, '2021_03_04_181646_add_foreign_keys_to_T00200_Cargo_Organizacion_table', 1),
(12, '2021_03_04_181646_add_foreign_keys_to_T00210_Socio_table', 1),
(13, '2021_03_04_181646_add_foreign_keys_to_T00220_Socio_Cargo_Organizacion_table', 1),
(14, '2021_03_04_181646_add_foreign_keys_to_T99999_Auditoria_table', 1),
(15, '2021_03_04_181646_add_foreign_keys_to_T99999_Bitacora_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `T00100_Usuario`
--

CREATE TABLE `T00100_Usuario` (
  `Co_Usuario` bigint(20) UNSIGNED NOT NULL,
  `Nb_Usuario` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tx_Email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nu_Movil` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Tx_Clave` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tx_Patron` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Nu_Intentos` int(11) NOT NULL DEFAULT '0',
  `Fe_Recuperacion` datetime DEFAULT NULL,
  `St_Bloqueo` tinyint(1) DEFAULT '0',
  `St_Activo` tinyint(1) NOT NULL DEFAULT '1',
  `Co_Auditoria` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `T00100_Usuario`
--

INSERT INTO `T00100_Usuario` (`Co_Usuario`, `Nb_Usuario`, `Tx_Email`, `Nu_Movil`, `Tx_Clave`, `Tx_Patron`, `Nu_Intentos`, `Fe_Recuperacion`, `St_Bloqueo`, `St_Activo`, `Co_Auditoria`) VALUES
(1, 'Prof. Dorian Heller', 'beryl.rogahn@mcglynn.com', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(2, 'Drake Wuckert', 'usanford@yahoo.org', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(3, 'Ms. Adelle Kiehn', 'walter.deron@gmail.com', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(4, 'Miss Jennie Botsford', 'cjones@yahoo.com', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(5, 'Arne Sipes', 'shields.kaela@rice.biz', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(6, 'Lou Adams', 'vglover@hotmail.com', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(7, 'Daisha Wilderman', 'kemmer.aubrey@hotmail.com', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(8, 'Mr. Delmer Mante', 'hayley46@gusikowski.biz', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(9, 'Dr. Gabriella Bode DDS', 'xcarroll@koelpin.net', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL),
(10, 'William Oberbrunner', 'francisco70@yahoo.com', NULL, 'unet123', NULL, 0, NULL, 0, 1, NULL);

--
-- Triggers `T00100_Usuario`
--
DELIMITER $$
CREATE TRIGGER `usu` AFTER INSERT ON `T00100_Usuario` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    new.Co_Usuario, ', ',
    new.Nb_Usuario, ', ', 
    new.Tx_Email, ', ',
    new.Tx_Clave, ', ',
    new.Nu_Intentos , ', ',
    new.St_Bloqueo, ', ',
    new.St_Activo, ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `usu1` AFTER UPDATE ON `T00100_Usuario` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    new.Co_Usuario, ', ',
    new.Nb_Usuario, ', ', 
    new.Tx_Email, ', ',
    new.Tx_Clave, ', ',
    new.Nu_Intentos , ', ',
    new.St_Bloqueo, ', ',
    new.St_Activo, ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `usu3` BEFORE DELETE ON `T00100_Usuario` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    old.Co_Usuario, ', ',
    old.Nb_Usuario, ', ', 
    old.Tx_Email, ', ',
    old.Tx_Clave, ', ',
    old.Nu_Intentos , ', ',
    old.St_Bloqueo, ', ',
    old.St_Activo, ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `T00200_Cargo_Organizacion`
--

CREATE TABLE `T00200_Cargo_Organizacion` (
  `Co_Cargo_Organizacion` bigint(20) UNSIGNED NOT NULL,
  `Nb_Cargo_Organizacion` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `St_Activo` tinyint(1) NOT NULL DEFAULT '1',
  `Co_Auditoria` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `T00200_Cargo_Organizacion`
--

INSERT INTO `T00200_Cargo_Organizacion` (`Co_Cargo_Organizacion`, `Nb_Cargo_Organizacion`, `St_Activo`, `Co_Auditoria`) VALUES
(1, 'Gerente', 1, 1),
(2, 'Administrador', 1, 1),
(3, 'Vendedor', 1, 1);

--
-- Triggers `T00200_Cargo_Organizacion`
--
DELIMITER $$
CREATE TRIGGER `car_org` AFTER INSERT ON `T00200_Cargo_Organizacion` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00200_Cargo_Organizacion', NULL, 'INS', concat( new.Co_Cargo_Organizacion, ', ', new.Nb_Cargo_Organizacion ,', ', new.St_Activo, ', ', new.Co_Auditoria, ', ' ), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `car_org1` AFTER UPDATE ON `T00200_Cargo_Organizacion` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00200_Cargo_Organizacion', NULL, 'INS', concat( new.Co_Cargo_Organizacion, ', ', new.Nb_Cargo_Organizacion ,', ', new.St_Activo, ', ', new.Co_Auditoria, ', ' ), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `carg_org2` BEFORE DELETE ON `T00200_Cargo_Organizacion` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00200_Cargo_Organizacion', NULL, 'INS', concat( old.Co_Cargo_Organizacion, ', ', old.Nb_Cargo_Organizacion ,', ', old.St_Activo, ', ', old.Co_Auditoria, ', ' ), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `T00210_Socio`
--

CREATE TABLE `T00210_Socio` (
  `Co_Socio` bigint(20) UNSIGNED NOT NULL,
  `Nb_Socio` char(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Co_Identificacion` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tx_Email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Nu_Movil` char(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Nu_Calificacion` int(11) NOT NULL DEFAULT '0',
  `St_Bloqueo` tinyint(1) DEFAULT '0',
  `St_Activo` tinyint(1) NOT NULL DEFAULT '1',
  `Co_Auditoria` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `T00210_Socio`
--

INSERT INTO `T00210_Socio` (`Co_Socio`, `Nb_Socio`, `Co_Identificacion`, `Tx_Email`, `Nu_Movil`, `Nu_Calificacion`, `St_Bloqueo`, `St_Activo`, `Co_Auditoria`) VALUES
(1, 'Santiago Legros Sr.', 'S3228', 'farrell.maximo@yahoo.com', NULL, 0, 0, 1, NULL),
(2, 'Samir Baumbach', 'S7201', 'kaley71@hotmail.com', NULL, 0, 0, 1, NULL),
(3, 'Angel Wuckert', 'S7229', 'luna28@yahoo.com', NULL, 0, 0, 1, NULL),
(4, 'Miss Eve Torp', 'S9224', 'carmen34@bashirian.com', NULL, 0, 0, 1, NULL),
(5, 'Marcelle Muller V', 'S6504', 'wbeahan@streich.net', NULL, 0, 0, 1, NULL),
(6, 'Neva Boehm', 'S9729', 'terence03@rowe.biz', NULL, 0, 0, 1, NULL),
(7, 'Maribel Daugherty', 'S5424', 'giovani.pfannerstill@lang.com', NULL, 0, 0, 1, NULL),
(8, 'Felicita Langosh', 'S4118', 'mwiegand@bruen.com', NULL, 0, 0, 1, NULL),
(9, 'George Predovic', 'S9433', 'ucarter@yahoo.com', NULL, 0, 0, 1, NULL),
(10, 'Thalia Towne', 'S2880', 'liam.aufderhar@gmail.com', NULL, 0, 0, 1, NULL);

--
-- Triggers `T00210_Socio`
--
DELIMITER $$
CREATE TRIGGER `soc` AFTER INSERT ON `T00210_Socio` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    new.Co_Socio, ', ',
    new.Nb_Socio, ', ',
    new.Co_Identificacion, ', ', 
    new.Tx_Email, ', ',
    new.Nu_Calificacion, ', ',
    new.St_Activo , ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `soc1` AFTER UPDATE ON `T00210_Socio` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    new.Co_Socio, ', ',
    new.Nb_Socio, ', ',
    new.Co_Identificacion, ', ', 
    new.Tx_Email, ', ',
    new.Nu_Calificacion, ', ',
    new.St_Activo , ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `soc2` BEFORE DELETE ON `T00210_Socio` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    old.Co_Socio, ', ',
    old.Nb_Socio, ', ',
    old.Co_Identificacion, ', ', 
    old.Tx_Email, ', ',
    old.Nu_Calificacion, ', ',
    old.St_Activo , ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `T00220_Socio_Cargo_Organizacion`
--

CREATE TABLE `T00220_Socio_Cargo_Organizacion` (
  `Co_Socio_Cargo_Organizacion` bigint(20) UNSIGNED NOT NULL,
  `Co_Socio` bigint(20) UNSIGNED NOT NULL,
  `Co_Cargo_Organizacion` bigint(20) UNSIGNED NOT NULL,
  `Fe_Inicio` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Fe_Fin` date DEFAULT NULL,
  `St_Activo` tinyint(1) NOT NULL DEFAULT '1',
  `Co_Auditoria` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `T00220_Socio_Cargo_Organizacion`
--

INSERT INTO `T00220_Socio_Cargo_Organizacion` (`Co_Socio_Cargo_Organizacion`, `Co_Socio`, `Co_Cargo_Organizacion`, `Fe_Inicio`, `Fe_Fin`, `St_Activo`, `Co_Auditoria`) VALUES
(4, 10, 1, '2021-03-19 18:20:13', NULL, 1, 1),
(5, 3, 3, '2021-03-19 18:20:50', NULL, 1, 4),
(6, 8, 3, '2021-03-19 18:20:50', NULL, 1, 10),
(7, 9, 2, '2021-03-19 18:21:23', NULL, 1, 8),
(8, 5, 3, '2021-03-19 18:21:23', NULL, 1, 3),
(9, 2, 2, '2021-03-19 18:21:48', NULL, 1, 18),
(10, 1, 3, '2021-03-19 18:22:31', NULL, 1, 5);

--
-- Triggers `T00220_Socio_Cargo_Organizacion`
--
DELIMITER $$
CREATE TRIGGER `soc_car_org` AFTER INSERT ON `T00220_Socio_Cargo_Organizacion` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    new.Co_Socio_Cargo_Organizacion, ', ',
    new.Co_Socio, ', ', 
    new.Co_Cargo_Organizacion, ', ',
    new.Fe_Inicio, ', ',
    'null', ', ',
    new.St_Activo , ', ',
    new.Co_Auditoria , ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `soc_car_org1` AFTER UPDATE ON `T00220_Socio_Cargo_Organizacion` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    new.Co_Socio_Cargo_Organizacion, ', ',
    new.Co_Socio, ', ', 
    new.Co_Cargo_Organizacion, ', ',
    new.Fe_Inicio, ', ',
    'null', ', ',
    new.St_Activo , ', ',
    new.Co_Auditoria , ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `soc_car_org2` BEFORE DELETE ON `T00220_Socio_Cargo_Organizacion` FOR EACH ROW INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES (NULL, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', concat(
    old.Co_Socio_Cargo_Organizacion, ', ',
    old.Co_Socio, ', ', 
    old.Co_Cargo_Organizacion, ', ',
    old.Fe_Inicio, ', ',
    'null', ', ',
    old.St_Activo , ', ',
    old.Co_Auditoria , ', '
), NULL, '5', '1', NULL, NULL, CURRENT_TIMESTAMP, '0')
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `T99999_Auditoria`
--

CREATE TABLE `T99999_Auditoria` (
  `Co_Auditoria` bigint(20) UNSIGNED NOT NULL,
  `Nb_Tabla` char(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Co_Fila` bigint(20) UNSIGNED DEFAULT NULL,
  `Co_Tipo_Operacion` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tx_Sentencia` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Tx_Error` varchar(5000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Co_Auditoria_Auditoria` bigint(20) UNSIGNED DEFAULT NULL,
  `Co_Usuario` bigint(20) UNSIGNED NOT NULL,
  `Co_MAC` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Co_IP` char(40) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `Fe_Ins` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `St_Error` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `T99999_Auditoria`
--

INSERT INTO `T99999_Auditoria` (`Co_Auditoria`, `Nb_Tabla`, `Co_Fila`, `Co_Tipo_Operacion`, `Tx_Sentencia`, `Tx_Error`, `Co_Auditoria_Auditoria`, `Co_Usuario`, `Co_MAC`, `Co_IP`, `Fe_Ins`, `St_Error`) VALUES
(1, 'T99999_Auditoria', 2, 'ACTIVE', 'Provident aliquid nihil ut tempora aut eum occaecati. Vel corrupti quo architecto debitis. Et non illo similique sapiente voluptas maiores aliquam. Eum tempore aut aliquam recusandae ut.', NULL, NULL, 2, NULL, NULL, '2021-03-15 19:12:54', 0),
(2, 'T99999_Bitacora', 5, 'ACTIVE', 'Vitae vel labore adipisci quo ut. Aut cumque rem architecto et incidunt. Quo ipsam quod laboriosam ut.', NULL, NULL, 5, NULL, NULL, '2021-03-15 19:12:54', 0),
(3, 'T00220_Socio_Cargo_Organizacion', 8, 'INACTIVE', 'Atque delectus et nesciunt. Est omnis tenetur facere beatae et. Mollitia optio occaecati a voluptatem. Esse aut quia quo sit eum quidem. Natus soluta nemo impedit porro.', NULL, NULL, 0, NULL, NULL, '2021-03-15 19:12:54', 0),
(4, 'T00210_Socio', 6, 'UPD', 'Aspernatur eos molestiae sit et consequatur perferendis natus. Sunt nulla ut nihil provident iste veritatis. Sed quas illum nostrum aut non ut.', NULL, NULL, 8, NULL, NULL, '2021-03-15 19:12:54', 0),
(5, 'T00200_Cargo_Organizacion', 6, 'INS', 'Quia quia incidunt dolore ipsa voluptas. Natus voluptatem placeat illo saepe est aut. Soluta voluptatem dicta et. Dolorem consequatur iure saepe optio rerum similique perspiciatis.', NULL, NULL, 0, NULL, NULL, '2021-03-15 19:12:54', 0),
(6, 'T00200_Cargo_Organizacion', 3, 'INS', 'Pariatur harum eos neque occaecati ea. Maiores sapiente dolorem cupiditate. Odio reprehenderit omnis excepturi nihil consectetur eius.', NULL, NULL, 3, NULL, NULL, '2021-03-15 19:12:54', 0),
(7, 'T00220_Socio_Cargo_Organizacion', 3, 'INS', 'Neque corrupti velit in esse facilis explicabo illo et. Distinctio expedita quod architecto quia. Est quod aperiam non corporis.', NULL, NULL, 4, NULL, NULL, '2021-03-15 19:12:54', 0),
(8, 'T99999_Auditoria', 0, 'UPD', 'Ut a maxime est voluptatem quisquam omnis eos. Et iusto omnis et omnis sapiente odio magnam nostrum.', NULL, NULL, 3, NULL, NULL, '2021-03-15 19:12:54', 0),
(9, 'T00200_Cargo_Organizacion', 9, 'ACTIVE', 'Tenetur facere error occaecati qui similique omnis doloremque. Ut minima inventore itaque. Est nemo eligendi sint consequatur ut illo illo. Tempore sit nam vero quibusdam unde nesciunt totam et.', NULL, NULL, 0, NULL, NULL, '2021-03-15 19:12:54', 0),
(10, 'T00210_Socio', 4, 'ACTIVE', 'Ullam harum sapiente modi. Quod modi qui iste eligendi quisquam. Neque sint est dolores blanditiis quos omnis expedita consequatur. Consequatur id aut quasi eius ea.', NULL, NULL, 8, NULL, NULL, '2021-03-15 19:12:54', 0),
(11, 'T00200_Cargo_Organizacion', NULL, 'INS', 'INSERT', NULL, 5, 1, NULL, NULL, '2021-03-19 17:30:40', 0),
(12, 'T00200_Cargo_Organizacion', NULL, 'INS', '1, Gerente, 1, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:01:15', 0),
(13, 'T00200_Cargo_Organizacion', NULL, 'INS', '2, Administrador, 1, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:02:17', 0),
(14, 'T00200_Cargo_Organizacion', NULL, 'INS', '3, Vendedor, 1, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:02:17', 0),
(15, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '4, 10, 1, 2021-03-19 18:20:13, null, 1, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:20:13', 0),
(16, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '5, 3, 3, 2021-03-19 18:20:50, null, 1, 4, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:20:50', 0),
(17, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '6, 8, 3, 2021-03-19 18:20:50, null, 1, 10, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:20:50', 0),
(18, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '7, 9, 2, 2021-03-19 18:21:23, null, 1, 8, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:21:23', 0),
(19, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '8, 5, 3, 2021-03-19 18:21:23, null, 1, 3, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:21:23', 0),
(20, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '9, 2, 2, 2021-03-19 18:21:48, null, 1, 18, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:21:48', 0),
(21, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '10, 1, 3, 2021-03-19 18:22:31, null, 1, 5, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:22:31', 0),
(22, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, Alejandra Perez, hola@mundo.com, unet123, 0, 0, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:29:43', 0),
(23, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, Silvia Torres, S6669, mundo@hola.com, 0, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:33:59', 0),
(24, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '2, Drake Wuckert, usanford@yahoo.org, unet123, 0, 0, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:36:16', 0),
(25, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, Alejandra Perez, hola@mundo.com, unet123, 0, 0, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:36:31', 0),
(26, 'T00200_Cargo_Organizacion', NULL, 'INS', '1, Maneger, 1, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:38:37', 0),
(27, 'T00200_Cargo_Organizacion', NULL, 'INS', '1, Gerente, 1, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:38:49', 0),
(28, 'T00200_Cargo_Organizacion', NULL, 'INS', '4, Prueba, 1, 14, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:39:09', 0),
(29, 'T00200_Cargo_Organizacion', NULL, 'INS', '4, Prueba, 1, 14, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:39:15', 0),
(30, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, Silvia Torres, S6669, mundo@hola.org, 0, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:40:40', 0),
(31, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, Silvia Torres, S6669, mundo@hola.org, 0, 1, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:40:49', 0),
(32, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, 6, 1, 2021-03-19 18:41:19, null, 1, 9, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:41:19', 0),
(33, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, 6, 2, 2021-03-19 18:41:19, null, 1, 9, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:42:36', 0),
(34, 'T00220_Socio_Cargo_Organizacion', NULL, 'INS', '11, 6, 2, 2021-03-19 18:41:19, null, 1, 9, ', NULL, 5, 1, NULL, NULL, '2021-03-19 18:42:42', 0);

-- --------------------------------------------------------

--
-- Table structure for table `T99999_Bitacora`
--

CREATE TABLE `T99999_Bitacora` (
  `Co_Bitacora` bigint(20) UNSIGNED NOT NULL,
  `Co_Bitacora_Previo` bigint(20) UNSIGNED DEFAULT NULL,
  `Co_Usuario` bigint(20) UNSIGNED NOT NULL,
  `Fe_Ins` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `T99999_Bitacora`
--

INSERT INTO `T99999_Bitacora` (`Co_Bitacora`, `Co_Bitacora_Previo`, `Co_Usuario`, `Fe_Ins`) VALUES
(1, NULL, 3, '2021-03-15 19:43:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `T00100_Usuario`
--
ALTER TABLE `T00100_Usuario`
  ADD PRIMARY KEY (`Co_Usuario`),
  ADD UNIQUE KEY `Uk_Usuario_Nb_Usuario` (`Nb_Usuario`),
  ADD UNIQUE KEY `Uk_Usuario_Tx_Email` (`Tx_Email`),
  ADD KEY `FK_Usuario_Auditoria` (`Co_Auditoria`);

--
-- Indexes for table `T00200_Cargo_Organizacion`
--
ALTER TABLE `T00200_Cargo_Organizacion`
  ADD PRIMARY KEY (`Co_Cargo_Organizacion`),
  ADD KEY `FK_CargoOrganizacion_Auditoria` (`Co_Auditoria`);

--
-- Indexes for table `T00210_Socio`
--
ALTER TABLE `T00210_Socio`
  ADD PRIMARY KEY (`Co_Socio`),
  ADD UNIQUE KEY `Uk_Socio_Identificacion` (`Co_Identificacion`),
  ADD UNIQUE KEY `Uk_Socio_Tx_Email` (`Tx_Email`),
  ADD KEY `FK_Socio_Auditoria` (`Co_Auditoria`);

--
-- Indexes for table `T00220_Socio_Cargo_Organizacion`
--
ALTER TABLE `T00220_Socio_Cargo_Organizacion`
  ADD PRIMARY KEY (`Co_Socio_Cargo_Organizacion`),
  ADD UNIQUE KEY `UK_SocioCargoOrganizacion` (`Co_Socio`,`Co_Cargo_Organizacion`),
  ADD KEY `FK_SocioCargoOrganizacion_CargoOrganizacion` (`Co_Cargo_Organizacion`),
  ADD KEY `FK_SocioCargoOrganizacion_Auditoria` (`Co_Auditoria`);

--
-- Indexes for table `T99999_Auditoria`
--
ALTER TABLE `T99999_Auditoria`
  ADD PRIMARY KEY (`Co_Auditoria`),
  ADD KEY `FK_Auditoria_Auditoria` (`Co_Auditoria_Auditoria`);

--
-- Indexes for table `T99999_Bitacora`
--
ALTER TABLE `T99999_Bitacora`
  ADD PRIMARY KEY (`Co_Bitacora`),
  ADD KEY `FK_Bitacora_Bitacora` (`Co_Bitacora_Previo`),
  ADD KEY `FK_Bitacora_Usuario` (`Co_Usuario`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `T00100_Usuario`
--
ALTER TABLE `T00100_Usuario`
  MODIFY `Co_Usuario` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `T00200_Cargo_Organizacion`
--
ALTER TABLE `T00200_Cargo_Organizacion`
  MODIFY `Co_Cargo_Organizacion` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `T00210_Socio`
--
ALTER TABLE `T00210_Socio`
  MODIFY `Co_Socio` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `T00220_Socio_Cargo_Organizacion`
--
ALTER TABLE `T00220_Socio_Cargo_Organizacion`
  MODIFY `Co_Socio_Cargo_Organizacion` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `T99999_Auditoria`
--
ALTER TABLE `T99999_Auditoria`
  MODIFY `Co_Auditoria` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `T99999_Bitacora`
--
ALTER TABLE `T99999_Bitacora`
  MODIFY `Co_Bitacora` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `T00100_Usuario`
--
ALTER TABLE `T00100_Usuario`
  ADD CONSTRAINT `FK_Usuario_Auditoria` FOREIGN KEY (`Co_Auditoria`) REFERENCES `T99999_Auditoria` (`Co_Auditoria`);

--
-- Constraints for table `T00200_Cargo_Organizacion`
--
ALTER TABLE `T00200_Cargo_Organizacion`
  ADD CONSTRAINT `FK_CargoOrganizacion_Auditoria` FOREIGN KEY (`Co_Auditoria`) REFERENCES `T99999_Auditoria` (`Co_Auditoria`);

--
-- Constraints for table `T00210_Socio`
--
ALTER TABLE `T00210_Socio`
  ADD CONSTRAINT `FK_Socio_Auditoria` FOREIGN KEY (`Co_Auditoria`) REFERENCES `T99999_Auditoria` (`Co_Auditoria`);

--
-- Constraints for table `T00220_Socio_Cargo_Organizacion`
--
ALTER TABLE `T00220_Socio_Cargo_Organizacion`
  ADD CONSTRAINT `FK_SocioCargoOrganizacion_Auditoria` FOREIGN KEY (`Co_Auditoria`) REFERENCES `T99999_Auditoria` (`Co_Auditoria`),
  ADD CONSTRAINT `FK_SocioCargoOrganizacion_CargoOrganizacion` FOREIGN KEY (`Co_Cargo_Organizacion`) REFERENCES `T00200_Cargo_Organizacion` (`Co_Cargo_Organizacion`),
  ADD CONSTRAINT `FK_SocioCargoOrganizacion_Socio` FOREIGN KEY (`Co_Socio`) REFERENCES `T00210_Socio` (`Co_Socio`);

--
-- Constraints for table `T99999_Auditoria`
--
ALTER TABLE `T99999_Auditoria`
  ADD CONSTRAINT `FK_Auditoria_Auditoria` FOREIGN KEY (`Co_Auditoria_Auditoria`) REFERENCES `T99999_Auditoria` (`Co_Auditoria`);

--
-- Constraints for table `T99999_Bitacora`
--
ALTER TABLE `T99999_Bitacora`
  ADD CONSTRAINT `FK_Bitacora_Bitacora` FOREIGN KEY (`Co_Bitacora_Previo`) REFERENCES `T99999_Bitacora` (`Co_Bitacora`),
  ADD CONSTRAINT `FK_Bitacora_Usuario` FOREIGN KEY (`Co_Usuario`) REFERENCES `T00100_Usuario` (`Co_Usuario`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
