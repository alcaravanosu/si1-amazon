<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Auditoria;
use Faker\Generator as Faker;

$factory->define(Auditoria::class, function (Faker $faker) {
    return [
        
					'Nb_Tabla'=>$faker->randomElement($array = array('T99999_Auditoria', 'T00100_Usuario', 'T99999_Bitacora', 'T00200_Cargo_Organizacion', 'T00210_Socio', 'T00220_Socio_Cargo_Organizacion')), 
					'Co_Fila'=> $faker->randomDigit(),
					'Co_Tipo_Operacion'=>$faker->randomElement($array = array('INS','DEL','UPD','ACTIVE','INACTIVE')),
					'Tx_Sentencia'=> $faker->text(),
					'Co_Usuario'=> $faker->randomDigit()
    ];
});
