<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Socio;
use Faker\Generator as Faker;

$factory->define(Socio::class, function (Faker $faker) {
    return [
			'Nb_Socio'=> $faker->name,
			'Tx_Email'=> $faker->email,
		 	'Co_Identificacion'=>$faker->numerify('S####')
    ];
});
