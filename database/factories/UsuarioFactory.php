<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Usuario;
use Faker\Generator as Faker;

$factory->define(Usuario::class, function (Faker $faker) {
    return [
			'Nb_Usuario'=> $faker->name,
			'Tx_Email'=> $faker->email,
		 	'Tx_Clave'=>'unet123'
    ];
});
