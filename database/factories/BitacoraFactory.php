<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Bitacora;
use App\Ususario;
use Faker\Generator as Faker;

$factory->define(Bitacora::class, function (Faker $faker) {
    return [
			'Co_Bitacora_Previo'=>null,
			'Co_Usuario'=>  $faker->randomDigit()
        //
    ];
});
