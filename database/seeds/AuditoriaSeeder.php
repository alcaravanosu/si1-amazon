<?php

use Illuminate\Database\Seeder;
use Faker\Generator as Faker;

class AuditoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			factory( App\Auditoria::class,10 )->create();
    }
}
