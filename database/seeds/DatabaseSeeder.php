<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        $this->call(AuditoriaSeeder::class);
        $this->call(UsuarioSeeder::class);
        $this->call(BitacoraSeeder::class);
        $this->call(SocioSeeder::class);
    }
}
