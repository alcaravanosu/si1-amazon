<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT00210SocioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('T00210_Socio', function (Blueprint $table) {
            $table->bigIncrements('Co_Socio');
            $table->char('Nb_Socio', 100);
            $table->string('Co_Identificacion', 50)->unique('Uk_Socio_Identificacion');
            $table->string('Tx_Email', 128)->unique('Uk_Socio_Tx_Email');
            $table->char('Nu_Movil', 20)->nullable();
            $table->integer('Nu_Calificacion')->default(0);
            $table->boolean('St_Bloqueo')->nullable()->default(0);
            $table->boolean('St_Activo')->default(1);
            $table->unsignedBigInteger('Co_Auditoria')->nullable()->index('FK_Socio_Auditoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T00210_Socio');
    }
}
