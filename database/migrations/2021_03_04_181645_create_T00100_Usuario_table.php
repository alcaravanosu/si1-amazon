<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT00100UsuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('T00100_Usuario', function (Blueprint $table) {
            $table->bigIncrements('Co_Usuario');
            $table->char('Nb_Usuario', 100)->unique('Uk_Usuario_Nb_Usuario');
            $table->string('Tx_Email', 128)->unique('Uk_Usuario_Tx_Email');
            $table->char('Nu_Movil', 20)->nullable();
            $table->char('Tx_Clave', 100);
            $table->char('Tx_Patron', 20)->nullable();
            $table->integer('Nu_Intentos')->default(0);
            $table->dateTime('Fe_Recuperacion')->nullable();
            $table->boolean('St_Bloqueo')->nullable()->default(0);
            $table->boolean('St_Activo')->default(1);
            $table->unsignedBigInteger('Co_Auditoria')->nullable()->index('FK_Usuario_Auditoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T00100_Usuario');
    }
}
