<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT99999BitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('T99999_Bitacora', function (Blueprint $table) {
            $table->bigIncrements('Co_Bitacora');
            $table->unsignedBigInteger('Co_Bitacora_Previo')->index('FK_Bitacora_Bitacora')->nullable();
            $table->unsignedBigInteger('Co_Usuario')->index('FK_Bitacora_Usuario');
            $table->dateTime('Fe_Ins')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T99999_Bitacora');
    }
}
