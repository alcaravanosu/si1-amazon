<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT99999AuditoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('T99999_Auditoria', function (Blueprint $table) {
            $table->bigIncrements('Co_Auditoria');
            $table->char('Nb_Tabla');
            $table->unsignedBigInteger('Co_Fila')->nullable();
            $table->string('Co_Tipo_Operacion', 10);
            $table->string('Tx_Sentencia', 5000);
            $table->string('Tx_Error', 5000)->nullable();
            $table->unsignedBigInteger('Co_Auditoria_Auditoria')->nullable()->index('FK_Auditoria_Auditoria');
            $table->unsignedBigInteger('Co_Usuario');
            $table->char('Co_MAC', 1)->nullable();
            $table->char('Co_IP', 40)->nullable();
            $table->dateTime('Fe_Ins')->useCurrent();
            $table->boolean('St_Error')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T99999_Auditoria');
    }
}
