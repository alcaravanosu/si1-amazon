<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToT99999BitacoraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('T99999_Bitacora', function (Blueprint $table) {
            $table->foreign('Co_Bitacora_Previo', 'FK_Bitacora_Bitacora')->references('Co_Bitacora')->on('T99999_Bitacora')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('Co_Usuario', 'FK_Bitacora_Usuario')->references('Co_Usuario')->on('T00100_Usuario')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('T99999_Bitacora', function (Blueprint $table) {
            $table->dropForeign('FK_Bitacora_Bitacora');
            $table->dropForeign('FK_Bitacora_Usuario');
        });
    }
}
