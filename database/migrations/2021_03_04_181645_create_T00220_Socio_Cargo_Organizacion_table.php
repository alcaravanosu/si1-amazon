<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT00220SocioCargoOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('T00220_Socio_Cargo_Organizacion', function (Blueprint $table) {
            $table->bigIncrements('Co_Socio_Cargo_Organizacion');
            $table->unsignedBigInteger('Co_Socio');
            $table->unsignedBigInteger('Co_Cargo_Organizacion')->index('FK_SocioCargoOrganizacion_CargoOrganizacion');
            $table->dateTime('Fe_Inicio')->useCurrent();
            $table->date('Fe_Fin')->nullable();
            $table->boolean('St_Activo')->default(1);
            $table->unsignedBigInteger('Co_Auditoria')->index('FK_SocioCargoOrganizacion_Auditoria');
            $table->unique(['Co_Socio', 'Co_Cargo_Organizacion'], 'UK_SocioCargoOrganizacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T00220_Socio_Cargo_Organizacion');
    }
}
