<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToT99999AuditoriaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('T99999_Auditoria', function (Blueprint $table) {
            $table->foreign('Co_Auditoria_Auditoria', 'FK_Auditoria_Auditoria')->references('Co_Auditoria')->on('T99999_Auditoria')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('T99999_Auditoria', function (Blueprint $table) {
            $table->dropForeign('FK_Auditoria_Auditoria');
        });
    }
}
