<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToT00200CargoOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('T00200_Cargo_Organizacion', function (Blueprint $table) {
            $table->foreign('Co_Auditoria', 'FK_CargoOrganizacion_Auditoria')->references('Co_Auditoria')->on('T99999_Auditoria')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('T00200_Cargo_Organizacion', function (Blueprint $table) {
            $table->dropForeign('FK_CargoOrganizacion_Auditoria');
        });
    }
}
