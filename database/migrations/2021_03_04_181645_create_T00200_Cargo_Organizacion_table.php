<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateT00200CargoOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('T00200_Cargo_Organizacion', function (Blueprint $table) {
            $table->bigIncrements('Co_Cargo_Organizacion');
            $table->string('Nb_Cargo_Organizacion', 500);
            $table->boolean('St_Activo')->default(1);
            $table->unsignedBigInteger('Co_Auditoria')->nullable()->index('FK_CargoOrganizacion_Auditoria');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('T00200_Cargo_Organizacion');
    }
}
