<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeysToT00220SocioCargoOrganizacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('T00220_Socio_Cargo_Organizacion', function (Blueprint $table) {
            $table->foreign('Co_Auditoria', 'FK_SocioCargoOrganizacion_Auditoria')->references('Co_Auditoria')->on('T99999_Auditoria')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('Co_Cargo_Organizacion', 'FK_SocioCargoOrganizacion_CargoOrganizacion')->references('Co_Cargo_Organizacion')->on('T00200_Cargo_Organizacion')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('Co_Socio', 'FK_SocioCargoOrganizacion_Socio')->references('Co_Socio')->on('T00210_Socio')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('T00220_Socio_Cargo_Organizacion', function (Blueprint $table) {
            $table->dropForeign('FK_SocioCargoOrganizacion_Auditoria');
            $table->dropForeign('FK_SocioCargoOrganizacion_CargoOrganizacion');
            $table->dropForeign('FK_SocioCargoOrganizacion_Socio');
        });
    }
}
