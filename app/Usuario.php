<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = "T00100_Usuario";
		protected $fillable = ['Nb_Usuario', 'Tx_Email', 'Tx_Clave' ];
		public $timestamps = false;

		public function bitacora()
    {
        return $this->belongsTo(Bitacora::class);
    }
}
