<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auditoria extends Model
{
		public $timestamps = false;
    protected $table = "T99999_Auditoria";
    protected $fillable = ['Nb_Tabla', 'Co_Tipo_Operacion', 'Tx_Sentencia','Co_Usuario'];

}
