<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bitacora extends Model
{
		protected $table = "T99999_Bitacora";
		public $timestamps = false;
    //protected $fillable = [];
		
		public function usuario()
    {
        return $this->hasOne(Usuario::class);
    }
}
