<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Socio;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Redirect;

class SocioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $socios = Socio::all();
        return View::make('socios/index')->with('socios', $socios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('socios/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $socio = new Socio();
        $socio->Nb_Socio = $request->Nb_Socio;
        $socio->Co_Identificacion = $request->Co_Identificacion;
        $socio->Tx_Email = $request->Tx_Email;
        $socio->save();

        return Redirect::route('socios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $socio = Socio::where('Co_Socio', $id)->first();
        return View::make('socios/show')->with('socio', $socio);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $socio = Socio::where('Co_Socio', $id)->first();
        return View::make('socios/edit')->with('socio', $socio)->with('id', $id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $socio = Socio::where('Co_Socio', $id)->first();
        $socio->Nb_Socio = $request->Nb_Socio;
        $socio->Co_Identificacion = $request->Co_Identificacion;
        $socio->Tx_Email = $request->Tx_Email;
        $socio->save();

        return Redirect::route('socios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $socio = Socio::find($id)->delete();
        return Redirect::route('socios.index');
    }
}
