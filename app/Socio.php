<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Socio extends Model
{
    use SoftDeletes;

		public $timestamps = false;
    protected $table = "T00210_Socio";
    protected $fillable = ['Nb_Socio', 'Tx_Email', 'Co_Identificacion'];
    protected $primaryKey = 'Co_Socio';
    protected $dates = ['deleted_at'];
}
